
This is my file to document my changes and improvements to the login server and messaging service.


First step: Refactoring and organisation

- The first thing I did was to refactor and re-organise the file system that was given. I did this by sorting python files and scripts into Py_Files, databases into Databases and miscellaneous resources into the Resources folder.

Second step: Implementing password hashing and authentication

- Thus far I have only imported hashlib and crearted a simple verification function which hashes the passwords and crosschecks with the password inputed, now it only allows an user 
  to go the messaging site if they have a fitting password. I will rework this feature when SQL has been implemented.

Third step: Implementing SQLite and using database

- I created a code snippet that I commented out at the bottom of app.py that inserts bob and alice as base users in the database and then I have a function that calls upon the database and creates a list of all its usernames, through this the password function now checks with the usernames stored password hashes instead so there never is a plaintext version of the password stored on the server. 

- Further I added the functionality to add a new user, which adds the username inputed and password inputed(hashed in SHA256) to the database, currently logging in is not functional on users added and only for bob and alice which I am currently debugging


Security:

Due to the fact that all my inputs are put into prepared statements I believe that my SQL code is also fairly safe toward SQL-injections.

Therefore I belive my applications main issues with security is the fact that sessions arent currently logged and thus everyone can access all data once they log in, which is an obvious security flaw, however I did not have the time nor energy to implement this sadly.

The worst case scenario of an attacker trying to do something here would be getting into the accounts and adding messages, as they are currently unable to remove any mesages due to the lack of functionality there. In addition they cannot do the SQL injection to drop the tables either due to how the prepared statements are made.

You can never really know that your security is "good enough" as its a concept that is constantly evolving and as such it constantly needs improvement.


How to run:

Prerequisites:
pip install flask
pip install apsw
pip install pygments

then run python -m flask --app app run

You will find the server on localhost:5000

To access the database you can import this code inside python3 console:

import sqlite3
con = sqlite3.connect("Databases/tiny.db")
cur = con.cursor()
res = cur.execute("SELECT * FROM users")
res.fetchall()


