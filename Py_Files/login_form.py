from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
import hashlib


class LoginForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    submit = SubmitField('Submit')

    def verify(self,password):
        p = hashlib.sha256()
        innatePass = self.password.data
        p.update(innatePass.encode())
        if(p.hexdigest()==password):
            return True
        else:
            return False

